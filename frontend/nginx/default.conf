upstream backend {
    server backend:8000;
}

upstream scrapyd {
    server scrapyd:6800;
}

server {
    listen 80;

    gzip on;

    location / {
        root /usr/share/nginx/html;
        index index.html index.htm;
        try_files $uri $uri/ /index.html =404;
    }


    location /static/ {
        alias /usr/share/nginx/static/;
        autoindex on;

    }

    location /tasks/ {
        proxy_pass http://scrapyd/jobs;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;                              
        proxy_redirect off;
        sub_filter "/logs/" "/scrapyd/logs/";
        sub_filter_once off;
        sub_filter_types *;
    }

    location /api {
        rewrite /api/(.*) /$1 break;
        proxy_pass http://backend;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;                              
        proxy_redirect off;
        sub_filter "/cars/" "/api/cars/";
        sub_filter_once off;
        sub_filter_types *;
    }

    location /admin {
        proxy_pass http://backend/admin;
    }
}
